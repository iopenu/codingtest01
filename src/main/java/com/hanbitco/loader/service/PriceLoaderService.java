package com.hanbitco.loader.service;

import com.hanbitco.api.data.currency.domain.detail.PriceDetail;
import org.json.simple.JSONObject;

import java.io.IOException;

public interface PriceLoaderService {

	PriceDetail loadingData() throws IOException;
	JSONObject loadingJsonObjData() throws IOException;

}
