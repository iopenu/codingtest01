package com.hanbitco.loader.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hanbitco.api.data.currency.domain.detail.PriceDetail;
import com.hanbitco.loader.service.PriceLoaderService;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * @author BBN
 */

@Service
public class PriceLoaderServiceImpl implements PriceLoaderService {
	private Logger logger = LoggerFactory.getLogger(PriceLoaderService.class);

	@Value("${json.price.file}")
	private String priceFile;
	private final String DIR;

	public PriceLoaderServiceImpl() {
		DIR = this.getClass().getResource("/").getPath().replaceFirst("^/(.:/)", "$1");
	}

	@Override
	@Cacheable(cacheNames = "pricesCache")
	public PriceDetail loadingData() {

		PriceDetail priceDetail = null;
		try {
			String loadData = fileRead(priceFile);

			if (loadData.length() > 0) {
				//파싱을 시작한다
				logger.debug("읽어온파일을 파싱한다 | {}", loadData);
				priceDetail = getPriceDetailFromJSONString(loadData);
			} else {
				Exception e = new Exception("파일에 문제가 있음");
				throw e;
			}
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObj = (JSONObject) jsonParser.parse(loadData);

		} catch (IOException e) {
			logger.error("파일을 불러오는데 실패하였습니다.");
			//TODO 관리자에게 알람을 보낸다.
			e.printStackTrace();
		} catch (Exception e) {
			//TODO 관리자에게 알람을 보낸다.
			e.printStackTrace();
		}
		return priceDetail;
	}

	@Override
	@Cacheable(cacheNames = "pricesCache")
	public JSONObject loadingJsonObjData() throws IOException {
		JSONObject jsonObject = new JSONObject();
		try {
			String loadData = fileRead(priceFile);

			if (loadData.length() > 0) {
				//파싱을 시작한다
				JSONParser jsonParser = new JSONParser();
				jsonObject = (JSONObject) jsonParser.parse(loadData);
			} else {
				Exception e = new Exception("파일에 문제가 있음");
				throw e;
			}
		} catch (IOException e) {
			logger.error("파일을 불러오는데 실패하였습니다.");
			//TODO 관리자에게 알람을 보낸다.
			e.printStackTrace();
		} catch (Exception e) {
			//TODO 관리자에게 알람을 보낸다.
			e.printStackTrace();
		}
		return jsonObject;
	}

	public PriceDetail getPriceDetailFromJSONString(String jsonString) throws IOException {
		PriceDetail priceDetail = null;
		ObjectMapper mapper = new ObjectMapper();
		priceDetail = mapper.readValue(jsonString, PriceDetail.class);
		return priceDetail;
	}

	public String fileRead(String priceFile) throws IOException {

		Path path = Paths.get(DIR + priceFile);

		String returnData = "";
		FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.READ);

		ByteBuffer buffer = ByteBuffer.allocate(2048);
		Charset charset = Charset.defaultCharset();

		int byteCount;

		while (true) {
			byteCount = fileChannel.read(buffer);

			if (byteCount == -1)
				break;
			buffer.flip();
			returnData += charset.decode(buffer).toString();
			buffer.clear();
		}
		fileChannel.close();
		logger.debug("data : " + returnData);

		return returnData;
	}


}
