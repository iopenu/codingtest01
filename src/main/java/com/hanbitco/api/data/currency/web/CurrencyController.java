package com.hanbitco.api.data.currency.web;
import com.hanbitco.api.data.currency.domain.detail.PriceDetail;
import com.hanbitco.api.data.currency.domain.response.CurrencyEnum;
import com.hanbitco.loader.service.PriceLoaderService;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import com.hanbitco.api.data.currency.service.CurrencyService;

@RestController
@RequestMapping({"/api/v1/data", "/api/data"})
public class CurrencyController {
	private Logger logger = LoggerFactory.getLogger(CurrencyController.class);

	public CurrencyController(){
	}

	@Autowired
	PriceLoaderService priceLoaderService;

	@Autowired
	CurrencyService currencyService;

	@RequestMapping("/")
	String home(){
		return "Hello world.";
	}

	@RequestMapping(value = "/currency", method = RequestMethod.GET, produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseEntity<?> getCurruncysLastPrice(HttpServletRequest request) throws IOException {
		request.getHeader("ContentType");
		Map<String, Object> map = new HashMap<>();

		JSONObject currencysLastPriceObj = currencyService.getCurruncysLastPrice();

		map.put("data",currencysLastPriceObj);
		map.put("status","success");
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/currency/{currency}", method = RequestMethod.GET, produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseEntity<?> getCurruncyLastPrice(HttpServletRequest request, @PathVariable("currency") String currency) throws IOException {
		request.getHeader("ContentType");
		Map<String, Object> map = new HashMap<>();

		String currencyName = CurrencyEnum.valueOf(currency).getName();
		JSONObject currencyLastPrice = currencyService.getCurruncyLastPrice(currencyName);

		map.put("status","success");
		map.put("data",currencyLastPrice);
		return new ResponseEntity<>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/data", method = RequestMethod.GET, produces="application/json; charset=UTF-8")
	public @ResponseBody ResponseEntity<?> getCurrenyData(HttpServletRequest request) throws IOException {
		request.getHeader("ContentType");
		Map<String, Object> map = new HashMap<>();

		PriceDetail priceDetail = priceLoaderService.loadingData();

		if(null!=priceDetail){
			map.put("data",priceDetail);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}
}
