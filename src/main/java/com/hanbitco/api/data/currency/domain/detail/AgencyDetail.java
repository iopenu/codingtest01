package com.hanbitco.api.data.currency.domain.detail;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgencyDetail {
	private String convertedPair;
	private String exchange;
	private String high;
	private String last;
	private String low;
	private String originPair;
	private AgencyRawDetail raw;
	private String timestamp;
}
