package com.hanbitco.api.data.currency.domain.response;

public enum AgentEnum {
	bitfinex(1)
	,bithumb(2)
	,coinone(3)
	,korbit(4);

	private int name;

	public int getName() {
		return name;
	}

	AgentEnum(int name){
		this.name = name;
	}


}
