package com.hanbitco.api.data.currency.service;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.Map;

public interface CurrencyService {
	JSONObject  getCurruncysLastPrice() throws IOException;
	JSONObject getCurruncyLastPrice(String currencyName) throws IOException;
}
