package com.hanbitco.api.data.currency.domain.detail;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceDetail {
	@JsonProperty("BCH_BTC")
	private Agencys bch_btc;
	@JsonProperty("EOS_BTC")
	private Agencys eos_btc;
	@JsonProperty("ETH_BTC")
	private Agencys eth_btc;
	@JsonProperty("LTC_BTC")
	private Agencys ltc_btc;
	@JsonProperty("XRP_BTC")
	private Agencys xrp_btc;

	@JsonProperty("BCH_KRW")
	private Agencys bch_krw;
	@JsonProperty("BTC_KRW")
	private Agencys btc_krw;
	@JsonProperty("BTG_KRW")
	private Agencys btg_krw;
	@JsonProperty("EOS_KRW")
	private Agencys eos_krw;
	@JsonProperty("ETH_KRW")
	private Agencys eth_krw;
	@JsonProperty("LTC_KRW")
	private Agencys ltc_krw;
	@JsonProperty("XRP_KRW")
	private Agencys xrp_krw;
}
