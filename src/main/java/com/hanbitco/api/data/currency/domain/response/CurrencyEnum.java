package com.hanbitco.api.data.currency.domain.response;

public enum CurrencyEnum {
	BTC("BTC_KRW")
	,LTC("LTC_KRW")
	,EOS("EOS_KRW")
	,BCH("BCH_KRW")
	,XRP("XRP_KRW");

	private String name;

	public String getName() {
		return name;
	}

	CurrencyEnum(String name){
		this.name = name;
	}

//
//	public String valueOfCurrency(String key){
//		String currency = null;
//		switch (key){
//			case "BTC":
//				currency = CurrencyEnum.BTC;
//				break;
//			case "LTC":
//				currency = CurrencyEnum.LTC.value;
//				break;
//			case "EOS":
//				currency = CurrencyEnum.EOS.value;
//				break;
//			case "BCH":
//				currency = CurrencyEnum.BCH.value;
//				break;
//			case "XRP":
//				currency = CurrencyEnum.XRP.value;
//				break;
//		}
//		return currency;
//	}
}
