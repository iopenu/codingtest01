package com.hanbitco.api.data.currency.domain.detail;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AgencyRawDetail {

	@JsonProperty("24H_fluctate")
	private int fluctate_24H;
	@JsonProperty("24H_fluctate_rate")
	private String fluctate_rate_24H;
	private String average_price;
	private String buy_price;
	private String date;
	private String max_price;
	private String min_price;
	private String opening_price;
	private String sell_price;
	private String units_traded;
	private String volume_1day;
	private String volume_7day;

	private String ask;
	private String bid;
	private String high;
	private String last_price;
	private String low;
	private String mid;
	private String timestamp;
	private String volume;


}
