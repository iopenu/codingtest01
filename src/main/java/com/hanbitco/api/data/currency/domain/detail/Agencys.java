package com.hanbitco.api.data.currency.domain.detail;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Agencys {
	private AgencyDetail bitfinex;
	private AgencyDetail bithumb;
	private AgencyDetail coinone;
	private AgencyDetail korbit;
}
