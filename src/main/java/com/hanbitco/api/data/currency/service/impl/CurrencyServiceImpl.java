package com.hanbitco.api.data.currency.service.impl;

import com.hanbitco.api.data.currency.domain.response.AgentEnum;
import com.hanbitco.api.data.currency.domain.response.CurrencyEnum;
import com.hanbitco.api.data.currency.service.CurrencyService;
import com.hanbitco.loader.service.PriceLoaderService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.DecimalFormat;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	PriceLoaderService priceLoaderService;

	public CurrencyServiceImpl() {
	}

	@Override
	public JSONObject getCurruncysLastPrice() throws IOException {
		JSONObject returnObject = new JSONObject();

		for (CurrencyEnum currencyType : CurrencyEnum.values()){
			JSONObject currencyLastPrice = getCurruncyLastPrice(currencyType.getName());
			returnObject.put(currencyType.getName(),currencyLastPrice);
		}
		return returnObject;
	}

	@Override
	public JSONObject getCurruncyLastPrice(String currencyName) throws IOException {
		JSONObject returnObject = new JSONObject();

		JSONObject allLoadingData =  priceLoaderService.loadingJsonObjData();

		JSONObject selectedCurrency = (JSONObject) allLoadingData.get(currencyName);
		for(AgentEnum type : AgentEnum.values()){
			JSONObject agentInfo = (JSONObject)selectedCurrency.get(type.toString());
			if(null==agentInfo){
				returnObject.put(type,null);
			}else{
				JSONObject returnDetail = new JSONObject();
				String originPair = (String)agentInfo.get("originPair");

				String pattern = "0.0";
				DecimalFormat df = new DecimalFormat(pattern);

				returnDetail.put("originPair",originPair);
				returnDetail.put("last",df.format(Float.parseFloat((String)agentInfo.get("last"))));

				returnObject.put(type,returnDetail);
			}
		}
		return returnObject;
	}


}
